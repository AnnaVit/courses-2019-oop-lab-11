﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards
{

    class FrenchDeck
    { 
        private Card[] cards;

        public FrenchDeck()
        {
            this.cards = new Card[(Enum.GetNames(typeof(FrenchSeed)).Length) * Enum.GetNames(typeof(FrenchValue)).Length+4];
        }

        public Card this[FrenchSeed seed, FrenchValue value]
        {
            get
            {
                foreach (Card card in cards)
                {
                    if (card.Seed == seed.ToString() && card.Value == value.ToString())
                    {
                        return card;
                    }
                }
                return null;
            }
        }
        public void Initialize()
        {
            /*
             * == README ==
             * 
             * I due enumerati (ItalianSeed e ItalianValue) definiti in fondo a questo file rappresentano rispettivamente semi e valori
             * di un tipo mazzo di carte da gioco italiano.
             * 
             * Questo metodo deve inizializzare il mazzo (l'array cards) considerando tutte le combinazioni possibili.
             * 
             * Nota - Dato un generico enumerato MyEnum:
             *   a) è possibile ottenere il numero dei valori presenti con Enum.GetNames(typeof(MyEnum)).Length
             *   b) è possibile ottenere l'elenco dei valori presenti con (MyEnum[])Enum.GetValues(typeof(MyEnum))
             * 
             */


            FrenchSeed[] seeds = (FrenchSeed[])Enum.GetValues(typeof(FrenchSeed));
            FrenchValue[] values = (FrenchValue[])Enum.GetValues(typeof(FrenchValue));
            int i = 0;

            foreach (FrenchSeed seed in seeds)
            {
                foreach (FrenchValue value in values)
                {

                    cards[i] = new Card(value.ToString(), seed.ToString());
                    i++;

                }
            }

            for(int c = (Enum.GetNames(typeof(FrenchSeed)).Length * Enum.GetNames(typeof(FrenchValue)).Length); c< cards.Length; c++)
            {
                cards[c]=new Card("JOLLY", " ");
            }
        }

        public void Print()
        {
            /*
             * == README  ==
             * 
             * Questo metodo stampa tutte le carte presenti nel mazzo, con una propria rappresentazione a scelta.
             */

            foreach (Card card in cards)
            {
                Console.WriteLine(card.ToString());
            }
        }

    }

    enum FrenchSeed
    {
        PICCHE,
        QUADRI,
        FIORI,
        CUORI
    }

    enum FrenchValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        OTTO,
        NOVE,
        DIECI,
        JACK,
        REGINA,
        RE,
    }

}
   

   

    

