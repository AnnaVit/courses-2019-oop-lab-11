﻿using System;

namespace ComplexNumbers
{
    class ComplexNum
    {
        public ComplexNum(double re, double im)
        {
            Re = re;
            Im = im;
        }

        public double Re { get; private set; }
        public double Im { get; private set; }


        // Restituisce il modulo del numero complesso
        public double Module
        {
            get
            {
                return Math.Sqrt(Re + Im);
            }
        }

        // Restituisce il complesso coniugato del numero complesso (https://en.wikipedia.org/wiki/Complex_conjugate)
        public ComplexNum Conjugate
        {
            get
            {
                return new ComplexNum(Re, -Im);
            }
        }

        public static ComplexNum operator + (ComplexNum a, ComplexNum b)
        {
            return new ComplexNum(a.Re + b.Re, a.Im + b.Im);
        }

        public static ComplexNum operator -(ComplexNum a, ComplexNum b)
        {
            return new ComplexNum(a.Re - b.Re, a.Im - b.Im);
        }

        public static ComplexNum operator *(ComplexNum a, ComplexNum b)
        {
            return new ComplexNum((a.Re*b.Re)-(a.Im*b.Im), (a.Re*b.Im)+(a.Im*b.Re));
        }

        public static ComplexNum operator /(ComplexNum a, ComplexNum b)
        {
            return new ComplexNum(((a.Re * b.Re) + (a.Im * b.Im))/(Math.Pow(b.Re,2.0)+Math.Pow(b.Im,2.0)), ((a.Im * b.Re) - (a.Re * b.Im))/ (Math.Pow(b.Re, 2.0) + Math.Pow(b.Im, 2.0)));
        }

        // Restituisce una rappresentazione idonea per il numero complesso
        public override string ToString()
        {
            return $"(Re={Re}, Im={Im})";
        }
    }
}
